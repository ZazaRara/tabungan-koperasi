/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transaksi;

import java.sql.Connection;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks
 */
public class tutupBuku extends javax.swing.JInternalFrame {

    String user = "";
    Connection kon;
    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    TaskWorker task;
    TaskWorkerSimpan taskSimpan;

    /**
     * Creates new form tutupBuku
     */
    public tutupBuku(String us, Connection ko) {
        this.user = us;
        this.kon = ko;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        cekAwal();

    }

    private void cekAwal() {
        int y = komp.getIntSQL(kon, "SELECT COUNT(1) as jml FROM tabungan_periode WHERE periode = DATE_FORMAT(now() -  INTERVAL 1 MONTH, '%Y%m')");
        if (y == 1) {
            panelData.removeAll();
            panelData.add(pasif);
        } else {
            int tahun = komp.getIntSQL(kon, "SELECT DATE_FORMAT(now() -  INTERVAL 1 MONTH, '%Y') as tahun");
            int bulan = komp.getIntSQL(kon, "SELECT DATE_FORMAT(now() -  INTERVAL 1 MONTH, '%m') as tahun");
            tBulan.setMonth(bulan - 1);
            tTahun.setYear(tahun);
            panelSimpan.setVisible(false);
            panelTabel.removeAll();

        }
    }

    private void proses() {

        String sql = "SELECT a.norek as id, a.norek, b.nama, IFNULL(MIN(saldo), 0) as saldo, c.bunga,\n"
                + "CASE WHEN MIN(saldo) >= 25000 THEN CAST(((c.bunga/12)/30 * MIN(saldo) * (DAYOFMONTH(LAST_DAY(now()-INTERVAL 1 MONTH))))/100 as DECIMAL(12,2)) ELSE 0 END as bunga_hasil\n"
                + "FROM tabungan a \n"
                + "LEFT JOIN langgan b ON b.no_rek = a.norek\n"
                + "LEFT JOIN bunga c ON c.jenis = b.jenis\n"
                + "WHERE tanggal >= LAST_DAY(now() - INTERVAL 2 MONTH) GROUP BY a.norek, b.nama;";
        komp.setDataTabel(kon, tabelData, sql, 1);
    }

    private void prosesSimpan() {

        int yy = tabelData.getRowCount();
        //    System.out.println(yy);
        int gagal = 0;
        int sukses = 0;
        int skip = 0;
        DecimalFormat df = new DecimalFormat("#.00");
        String tanggal = komp.getStringSQL(kon, "SELECT LAST_DAY(now() - INTERVAL 1 MONTH)");
        System.out.println(yy);
        for (int x = 0; x < yy; x++) {
            String norek = tabelData.getValueAt(x, 1).toString();
            //  String bNorek = tabelData.getValueAt(x, 2).toString();
            String hasil = tabelData.getValueAt(x, 6).toString();
            String saldo_akhir = komp.getStringSQL(kon, "SELECT IFNULL(saldo, 0) as saldo FROM tabungan WHERE norek = '" + norek + "' ORDER BY id desc LIMIT 1");
            float saldo = Float.parseFloat(saldo_akhir) + Float.parseFloat(hasil);
            try {
                String aha = df.format(saldo).replaceAll(",", ".");
                System.out.println(x + " norek : " + norek + " : " + df.format(saldo) + "  -  " + saldo_akhir);
                String save = "INSERT INTO tabungan"
                        + "(norek, bukti, tanggal, masuk, keluar, saldo, keterangan, create_date, create_user) "
                        + "VALUES('" + norek + "','AUTO','" + tanggal + "',"
                        + "'" + hasil + "','0','" + aha + "','BUNGA',now(),'" + user + "')";

                boolean ok = komp.setSQL(kon, save);
                if (ok) {
                    sukses++;
                } else {
                    gagal++;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        String periode = tanggal.replaceAll("-", "");
        periode = periode.substring(0, 6);
        String is = "INSERT INTO tabungan_periode(periode, create_date, create_user) VALUES('" + periode + "',now(),'" + user + "')";
        komp.setSQL(kon, is);
        pesan.pesanSukses("Sukses Simpan Data Bunga", "<br />Total Data :" + yy + "<br />Sukses : " + sukses + "<br />Gagal : " + gagal);
        cekAwal();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pasif = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        panelBunga = new javax.swing.JScrollPane();
        tabelData = new javax.swing.JTable();
        panelTunggu = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        panelSimpanData = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        xxyyy = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        panelData = new javax.swing.JPanel();
        data = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        tBulan = new com.toedter.calendar.JMonthChooser();
        tTahun = new com.toedter.calendar.JYearChooser();
        btnProses = new javax.swing.JButton();
        panelTabel = new javax.swing.JPanel();
        panelSimpan = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        pasif.setBackground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 255));
        jLabel9.setText("TRANSAKSI TUTUP BUKU BULAN KEMARIN SUDAH DILAKUKAN");
        pasif.add(jLabel9);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 0, 51));
        jLabel6.setText("SILAHKAN LAKUKAN TRANSAKSI TABUNGAN");
        pasif.add(jLabel6);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 0, 255));
        jLabel10.setText("PROSES TUTUP BUKU DAPAT DILAKUKAN");
        pasif.add(jLabel10);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 255));
        jLabel11.setText("SETELAH TGL 1 BULAN BERJALAN");
        pasif.add(jLabel11);

        tabelData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Nomor Rekening", "Nama Anggota", "Saldo Minimum", "% Bunga", "Hasil Bunga"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        panelBunga.setViewportView(tabelData);

        panelTunggu.setOpaque(false);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 40)); // NOI18N
        jLabel3.setText("PROSES PERHITUNGAN SEDANG BERLANGSUNG");
        panelTunggu.add(jLabel3);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 55)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 204));
        jLabel4.setText("MOHON MENUNGGU............");
        panelTunggu.add(jLabel4);

        panelSimpanData.setOpaque(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 40)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 102, 102));
        jLabel5.setText("PROSES SIMPAN DATA BUNGA SEDANG BERLANGSUNG");
        panelSimpanData.add(jLabel5);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 55)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 204));
        jLabel7.setText("MOHON MENUNGGU............");
        panelSimpanData.add(jLabel7);

        setTitle("Tutup Buku");

        xxyyy.setBackground(new java.awt.Color(255, 255, 255));
        xxyyy.setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));
        jPanel2.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("PROSES TUTUP BUKU");
        jPanel2.add(jLabel1);

        xxyyy.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setOpaque(false);
        xxyyy.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jPanel4.setOpaque(false);
        jPanel4.setPreferredSize(new java.awt.Dimension(100, 10));
        xxyyy.add(jPanel4, java.awt.BorderLayout.LINE_END);

        jPanel5.setOpaque(false);
        jPanel5.setPreferredSize(new java.awt.Dimension(100, 10));
        xxyyy.add(jPanel5, java.awt.BorderLayout.LINE_START);

        panelData.setOpaque(false);
        panelData.setLayout(new java.awt.BorderLayout());

        data.setOpaque(false);
        data.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("PERIODE TUTUP BUKU");
        data.add(jLabel2);

        tBulan.setDayChooser(null);
        tBulan.setEnabled(false);
        tBulan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tBulan.setYearChooser(tTahun);
        data.add(tBulan);

        tTahun.setDayChooser(null);
        tTahun.setEnabled(false);
        data.add(tTahun);

        btnProses.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnProses.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/calculate.png"))); // NOI18N
        btnProses.setText("   P R O S E S  ");
        btnProses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProsesActionPerformed(evt);
            }
        });
        data.add(btnProses);

        panelData.add(data, java.awt.BorderLayout.NORTH);

        panelTabel.setOpaque(false);
        panelTabel.setLayout(new java.awt.GridLayout(1, 0));
        panelData.add(panelTabel, java.awt.BorderLayout.CENTER);

        panelSimpan.setOpaque(false);
        panelSimpan.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 20, 20));

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton2.setText("SIMPAN DATA");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        panelSimpan.add(jButton2);

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jButton3.setText("BATAL");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        panelSimpan.add(jButton3);

        panelData.add(panelSimpan, java.awt.BorderLayout.PAGE_END);

        xxyyy.add(panelData, java.awt.BorderLayout.CENTER);

        getContentPane().add(xxyyy, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnProsesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProsesActionPerformed
        panelTabel.removeAll();
        panelTabel.add(panelTunggu);
        btnProses.setEnabled(false);
        task = new TaskWorker();
        task.execute();
    }//GEN-LAST:event_btnProsesActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int y = JOptionPane.showConfirmDialog(this, "Anda ingin membatalkan hasil perhitungan bunga? Jika yajin silahkan pilih YES", "Konfirmasi", JOptionPane.YES_NO_OPTION);
        if (y == JOptionPane.YES_OPTION) {
            btnProses.setEnabled(true);
            panelSimpan.setVisible(false);
            panelTabel.removeAll();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int y = JOptionPane.showConfirmDialog(this, "Anda ingin menyimpan data perhitungan bunga? Jika yajin silahkan pilih YES", "Konfirmasi", JOptionPane.YES_NO_OPTION);
        if (y == JOptionPane.YES_OPTION) {
            btnProses.setEnabled(false);
            panelSimpan.setVisible(false);
            panelTabel.removeAll();
            panelTabel.add(panelSimpanData);

            taskSimpan = new TaskWorkerSimpan();
            taskSimpan.execute();
        }
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnProses;
    private javax.swing.JPanel data;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane panelBunga;
    private javax.swing.JPanel panelData;
    private javax.swing.JPanel panelSimpan;
    private javax.swing.JPanel panelSimpanData;
    private javax.swing.JPanel panelTabel;
    private javax.swing.JPanel panelTunggu;
    private javax.swing.JPanel pasif;
    private com.toedter.calendar.JMonthChooser tBulan;
    private com.toedter.calendar.JYearChooser tTahun;
    private javax.swing.JTable tabelData;
    private javax.swing.JPanel xxyyy;
    // End of variables declaration//GEN-END:variables
  class TaskWorker extends SwingWorker<Void, Void> {

        /*
     * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
            proses();
            return null;
        }

        /*
     * Executed in event dispatching thread
         */
        @Override
        public void done() {
            panelSimpan.setVisible(true);
            panelTabel.removeAll();
            panelTabel.add(panelBunga);

            //    atas.removeAll();
            //    atas.add(panelFilter);
        }
    }

    class TaskWorkerSimpan extends SwingWorker<Void, Void> {

        /*
     * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
            prosesSimpan();
            return null;
        }

        /*
     * Executed in event dispatching thread
         */
        @Override
        public void done() {
            //   panelSimpan.setVisible(true);

            //      cekAwal();
            //    panelTabel.add(panelBunga);
            //    atas.removeAll();
            //    atas.add(panelFilter);
        }
    }
}
