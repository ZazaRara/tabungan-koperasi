/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transaksi;

import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Calendar;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks
 */
public class tabungan extends javax.swing.JInternalFrame {

    String user = "";
    Connection kon;
    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();

    /**
     * Creates new form tabungan
     */
    public tabungan(String us, Connection ko) {
        this.user = us;
        this.kon = ko;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        cek();

    }

    private void cek() {
//        int y = komp.getIntSQL(kon, "SELECT COUNT(1) as jml FROM tabungan_periode WHERE periode = DATE_FORMAT(now()  -  INTERVAL 1 MONTH, '%Y%m')");
//        if (y == 0) {
//            panelData.removeAll();
//            panelData.add(pasif);
//        } else {
        btnSimpan.setVisible(false);
        btnBatal.setVisible(false);
        btnBaru.setVisible(false);

        String peri = komp.getStringSQL(kon, "SELECT periode FROM tabungan_periode ORDER BY create_date DESC LIMIT 1");

        String yy = peri.substring(0, 4);
        String bb = peri.substring(4, 6);

        int bln_aktif = Integer.parseInt(bb) + 1;
        int thn_aktif = Integer.parseInt(yy);
        if (bln_aktif > 13) {
            bln_aktif = 1;
            thn_aktif = thn_aktif + 1;
        }
        Calendar date = Calendar.getInstance();
        date.set(thn_aktif, bln_aktif - 1, 1);
        tTanggal.setDate(date.getTime());
        tTanggal.getJCalendar().setMinSelectableDate(date.getTime());
        //  System.out.println(date.getActualMaximum(date.DAY_OF_MONTH));
        date.set(thn_aktif, bln_aktif - 1, date.getActualMaximum(date.DAY_OF_MONTH));
        tTanggal.getJCalendar().setMaxSelectableDate(date.getTime());
//        }
    }

    private void showData() {
        String nor = dNorek.getText().trim();
        String nama = komp.getStringSQL(kon, "SELECT nama FROM langgan WHERE no_rek = '" + nor + "'");
        String saldo = komp.getStringSQL(kon, "SELECT FORMAT(saldo,2) as saldo FROM tabungan WHERE norek = '" + nor + "' ORDER BY tanggal DESC LIMIT 1");
        if (nama.length() > 0) {
            String no = komp.getStringSQL(kon, "SELECT max(id) + 1 as jml FROM tabungan");
            pNama.setText(nama);
            pSaldo.setText(saldo);
            tJenis.setSelectedIndex(0);
            tJumlah.setText("0");
            tNominalJumlah.setText("0");
            tBukti.setText(nor.substring(nor.length() - 3, nor.length()) + "/" + no);
            tKeterangan.setText("");
            dNorek.setEnabled(false);
            btnSimpan.setVisible(true);
            btnBatal.setVisible(true);
            btnBaru.setVisible(false);
            String sql = "SELECT v.* FROM (SELECT id, tanggal, keterangan, bukti, FORMAT(masuk,2) as masuk, FORMAT(keluar,2) as keluar, FORMAT(saldo,2) as saldo FROM tabungan WHERE norek = '" + nor + "' and keterangan != 'O' ORDER BY tanggal DESC LIMIT 25) v ORDER BY v.id";
            komp.setDataTabel(kon, tabelTabungan, sql, 1);
        } else {
            pesan.pesanError("Nomor Rekening tidak ditemukan", "Periksa nomor rekening yang dimasukkan", "Silahkan ulangi lagi");
        }
    }

    private void simpanData() {
        DecimalFormat df = new DecimalFormat("#.00");
        String norek = dNorek.getText();
        String jenis = tJenis.getSelectedItem().toString();
        String tgl = new java.sql.Date(tTanggal.getDate().getTime()).toString();
        String bukti = tBukti.getText();
        String nom = tJumlah.getText();
        String ket = tKeterangan.getText();
        boolean ok = false;
        if (norek.length() == 0 || bukti.length() == 0 || nom.length() <= 2) {
            pesan.pesanError("Gagal Simpan Data", "Data Belum lengkap", "Mohon lengkapi semua data");
        } else {
            String saldo_akhir = komp.getStringSQL(kon, "SELECT IFNULL(saldo, 2) as saldo FROM tabungan WHERE norek = '" + norek + "' ORDER BY id desc LIMIT 1");

            if (jenis.equalsIgnoreCase("SETOR") || jenis.equalsIgnoreCase("PENAMBAHAN") || jenis.equalsIgnoreCase("POTONG GAJI")) {
                float saldo = Float.parseFloat(saldo_akhir) + Float.parseFloat(nom);
                String aha = df.format(saldo).replaceAll(",", ".");
                String save = "INSERT INTO tabungan"
                        + "(norek, bukti, tanggal, masuk, keluar, saldo, keterangan, create_date, create_user) "
                        + "VALUES('" + norek + "','" + bukti + "','" + tgl + "',"
                        + "'" + nom + "','0','" + aha + "','" + jenis + "',now(),'" + user + "')";
                ok = komp.setSQL(kon, save);
            } else if (jenis.equalsIgnoreCase("TARIK") || jenis.equalsIgnoreCase("PENGURANGAN")) {
                float saldo = Float.parseFloat(saldo_akhir) - Float.parseFloat(nom);
                if (saldo < 0) {
                    pesan.pesanError("Gagal Tarik Uang", "Terjadi kesalahan", "Saldo Anda tidak mencukupi");
                } else {
                    String aha = df.format(saldo).replaceAll(",", ".");
                    String save = "INSERT INTO tabungan"
                            + "(norek, bukti, tanggal, keluar, masuk, saldo, keterangan, create_date, create_user) "
                            + "VALUES('" + norek + "','" + bukti + "','" + tgl + "',"
                            + "'" + nom + "','0','" + aha + "','" + jenis + "',now(),'" + user + "')";
                    ok = komp.setSQL(kon, save);
                }
            }
            if (ok) {
                pesan.pesanSukses("Sukses Simpan Data", "Transaksi sukses disimpan");
                suksesSimpan();
            } else {
                pesan.pesanError("Gagal Simpan Data", "Terjadi kesalahan", "Mohon hub administrator");
            }
        }
    }

    private void showKelasSiswaBaru() {
        String a = this.filter_kelas_baru.getText();
        String[] data = {"no_rek",
            "nama",
            "section"};

        String where = komp.getStringFilter(data, a);
        String sql = "SELECT no_rek, no_rek as kode, nama, section  FROM langgan " + where + " ORDER BY nama LIMIT 100";
        //  System.out.println(sql);
        komp.setDataTabel(kon, tabel_baru_kelas, sql, 1);
    }

    private void suksesSimpan() {
        pNama.setEnabled(false);
        pSaldo.setEnabled(false);
        tJenis.setEnabled(false);
        tJumlah.setEnabled(false);
        tNominalJumlah.setEnabled(false);
        tBukti.setEnabled(false);
        tKeterangan.setEnabled(false);
        dNorek.setEnabled(false);
        btnSimpan.setVisible(false);
        btnBatal.setVisible(false);
        btnBaru.setVisible(true);
        String nor = dNorek.getText().trim();
        //   String nama = komp.getStringSQL(kon, "SELECT nama FROM langgan WHERE no_rek = '" + nor + "'");
        String saldo = komp.getStringSQL(kon, "SELECT FORMAT(saldo,2) as saldo FROM tabungan WHERE norek = '" + nor + "' ORDER BY tanggal DESC LIMIT 1");

        //    pNama.setText(nama);
        pSaldo.setText(saldo);
        String sql = "SELECT v.* FROM (SELECT id, tanggal, keterangan, bukti, FORMAT(masuk,2) as masuk, FORMAT(keluar,2) as keluar, FORMAT(saldo,2) as saldo FROM tabungan WHERE norek = '" + nor + "' and keterangan != 'O' ORDER BY tanggal DESC LIMIT 25) v ORDER BY v.id";
        komp.setDataTabel(kon, tabelTabungan, sql, 1);
    }

    private void batal() {
        pNama.setText("");
        pSaldo.setText("");
        tJenis.setSelectedIndex(0);
        tJumlah.setText("0");
        tNominalJumlah.setText("0");
        tBukti.setText("");
        tKeterangan.setText("");
        dNorek.setText("");
        dNorek.setEnabled(true);
        btnSimpan.setVisible(true);
        btnBatal.setVisible(true);
        btnBaru.setVisible(false);
        //  komp.hapusTabel(tabelTabungan);
    }

    private void baru() {

        pNama.setText("");
        pSaldo.setText("");
        tJenis.setSelectedIndex(0);
        tJumlah.setText("0");
        tNominalJumlah.setText("0");
        tBukti.setText("");
        tKeterangan.setText("");
        dNorek.setText("");
        pNama.setEnabled(true);
        pSaldo.setEnabled(true);
        tJenis.setEnabled(true);
        tJumlah.setEnabled(true);
        tNominalJumlah.setEnabled(true);
        tBukti.setEnabled(true);
        tKeterangan.setEnabled(true);
        dNorek.setEnabled(true);
        komp.hapusTabel(tabelTabungan);
        btnSimpan.setVisible(false);
        btnBatal.setVisible(false);
        btnBaru.setVisible(false);
    }

    private void pilihKelasSiswaBaru() {
        int y = tabel_baru_kelas.getSelectedRow();
        if (y >= 0) {
            String id = tabel_baru_kelas.getValueAt(y, 1).toString();
            dNorek.setText(id);
            showData();
            baru_kelas.dispose();

        } else {
            pesan.pesanError("ERROR", "Anggota belum dipilih", "Silahkan pilih anggota yang diinginkan");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pasif = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        baru_kelas = new javax.swing.JDialog();
        jPanel11 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabel_baru_kelas = new javax.swing.JTable();
        filter_kelas_baru = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        btn_pilih_kelas_baru = new javax.swing.JButton();
        btn_batal_kelas_baru = new javax.swing.JButton();
        xxyyy = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        panelData = new javax.swing.JPanel();
        data = new javax.swing.JPanel();
        dNorek = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnCari = new javax.swing.JButton();
        pNama = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        pSaldo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        tJenis = new javax.swing.JComboBox<>();
        tJumlah = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        tNominalJumlah = new javax.swing.JLabel();
        btnBatal = new javax.swing.JButton();
        btnSimpan = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        tBukti = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        tTanggal = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        tKeterangan = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelTabungan = new javax.swing.JTable();
        btnBaru = new javax.swing.JButton();

        pasif.setBackground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 255));
        jLabel9.setText("TRANSAKSI TABUNGAN TIDAK DAPAT DILANJUTKAN");
        pasif.add(jLabel9);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 0, 51));
        jLabel6.setText("BULAN LALU BELUM TUTUP BUKU !!!!");
        pasif.add(jLabel6);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 0, 204));
        jLabel10.setText("SILAHKAN LAKUKAN PROSES TUTUP BUKU");
        pasif.add(jLabel10);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 204));
        jLabel11.setText("SEBELUM MELAKUKAN TRANSAKSI BULAN INI");
        pasif.add(jLabel11);

        jButton4.setText("CEK ULANG");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        pasif.add(jButton4);

        baru_kelas.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        baru_kelas.setModal(true);
        baru_kelas.setUndecorated(true);
        baru_kelas.setResizable(false);

        jPanel11.setBackground(new java.awt.Color(255, 204, 255));
        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "  Data Kelas  "));
        jPanel17.setOpaque(false);
        jPanel17.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabel_baru_kelas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "2016-03-05", "07:00", "1", null}
            },
            new String [] {
                "No", "id", "No. Rekening", "Nama Anggota", "Section"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabel_baru_kelas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabel_baru_kelasMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tabel_baru_kelas);

        jPanel17.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 700, 310));

        jPanel11.add(jPanel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 740, 350));

        filter_kelas_baru.setPreferredSize(new java.awt.Dimension(60, 25));
        filter_kelas_baru.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filter_kelas_baruKeyReleased(evt);
            }
        });
        jPanel11.add(filter_kelas_baru, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 350, -1));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/reset.png"))); // NOI18N
        jButton3.setText("Filter");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel11.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, -1, -1));

        baru_kelas.getContentPane().add(jPanel11, java.awt.BorderLayout.CENTER);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jPanel12.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/icon.jpg"))); // NOI18N
        jPanel12.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, 90));

        jLabel13.setBackground(new java.awt.Color(255, 51, 51));
        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 51, 51));
        jLabel13.setText("Daftar Anggota");
        jPanel12.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 0, 390, 110));

        baru_kelas.getContentPane().add(jPanel12, java.awt.BorderLayout.NORTH);

        jPanel13.setBackground(new java.awt.Color(255, 51, 51));
        jPanel13.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        btn_pilih_kelas_baru.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        btn_pilih_kelas_baru.setText("Pilih");
        btn_pilih_kelas_baru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_pilih_kelas_baruActionPerformed(evt);
            }
        });
        jPanel13.add(btn_pilih_kelas_baru);

        btn_batal_kelas_baru.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        btn_batal_kelas_baru.setText("Tutup");
        btn_batal_kelas_baru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batal_kelas_baruActionPerformed(evt);
            }
        });
        jPanel13.add(btn_batal_kelas_baru);

        baru_kelas.getContentPane().add(jPanel13, java.awt.BorderLayout.SOUTH);

        setTitle("Tabungan");

        xxyyy.setBackground(new java.awt.Color(255, 255, 255));
        xxyyy.setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));
        jPanel2.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("TRANSAKSI TABUNGAN");
        jPanel2.add(jLabel1);

        xxyyy.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setOpaque(false);
        xxyyy.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jPanel4.setOpaque(false);
        xxyyy.add(jPanel4, java.awt.BorderLayout.LINE_END);

        jPanel5.setOpaque(false);
        xxyyy.add(jPanel5, java.awt.BorderLayout.LINE_START);

        panelData.setOpaque(false);
        panelData.setLayout(new java.awt.GridLayout(1, 0));

        data.setOpaque(false);
        data.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        dNorek.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        dNorek.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dNorekKeyReleased(evt);
            }
        });
        data.add(dNorek, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 220, 30));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Nomor Rekening");
        data.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 270, -1));

        btnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/cari.png"))); // NOI18N
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });
        data.add(btnCari, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 50, -1, 30));

        pNama.setEnabled(false);
        data.add(pNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 220, -1));

        jLabel3.setText("Nama Anggota");
        data.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 180, -1));

        pSaldo.setEnabled(false);
        data.add(pSaldo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 140, -1));

        jLabel4.setText("Saldo Akhir");
        data.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 180, -1));

        tJenis.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tJenis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SETOR", "TARIK", "POTONG GAJI", "PENGURANGAN", "PENAMBAHAN" }));
        data.add(tJenis, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 190, 120, -1));

        tJumlah.setText("0");
        tJumlah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tJumlahKeyReleased(evt);
            }
        });
        data.add(tJumlah, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 110, -1));

        jLabel5.setText("Nominal");
        data.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, 180, -1));

        tNominalJumlah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tNominalJumlah.setText("0");
        data.add(tNominalJumlah, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 290, 200, -1));

        btnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        btnBatal.setText("BATAL");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        data.add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 390, -1, -1));

        btnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        btnSimpan.setText("SIMPAN");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });
        data.add(btnSimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, -1, -1));

        jLabel7.setText("Bukti");
        data.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 180, -1));
        data.add(tBukti, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 220, -1));
        data.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 370, 220, 20));

        tTanggal.setDate(new java.util.Date());
        tTanggal.setDateFormatString("dd/MM/yyyy");
        data.add(tTanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));

        jLabel8.setText("Keterangan");
        data.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 320, 180, -1));
        data.add(tKeterangan, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, 210, -1));
        data.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 220, 20));

        tabelTabungan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Tanggal", "Keterangan", "Bukti", "Masuk", "Keluar", "Saldo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabelTabungan);

        data.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 30, 560, 440));

        btnBaru.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/tambah.png"))); // NOI18N
        btnBaru.setText("BARU");
        btnBaru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBaruActionPerformed(evt);
            }
        });
        data.add(btnBaru, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, -1, -1));

        panelData.add(data);

        xxyyy.add(panelData, java.awt.BorderLayout.CENTER);

        getContentPane().add(xxyyy, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dNorekKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dNorekKeyReleased

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            showData();
        }
    }//GEN-LAST:event_dNorekKeyReleased

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        simpanData();   // TODO add your handling code here:
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        batal();        // TODO add your handling code here:
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        showKelasSiswaBaru();
        komp.showDialog(baru_kelas, 760, 550);
    }//GEN-LAST:event_btnCariActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        cek();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnBaruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBaruActionPerformed
        baru();
    }//GEN-LAST:event_btnBaruActionPerformed

    private void tJumlahKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tJumlahKeyReleased
        tNominalJumlah.setText(komp.ribuan(tJumlah.getText()));
    }//GEN-LAST:event_tJumlahKeyReleased

    private void tabel_baru_kelasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabel_baru_kelasMouseClicked

        int x = evt.getClickCount();
        if (x >= 2) {
            pilihKelasSiswaBaru();
        }
    }//GEN-LAST:event_tabel_baru_kelasMouseClicked

    private void filter_kelas_baruKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filter_kelas_baruKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            showKelasSiswaBaru();
        }
    }//GEN-LAST:event_filter_kelas_baruKeyReleased

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        showKelasSiswaBaru();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btn_pilih_kelas_baruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_pilih_kelas_baruActionPerformed

        pilihKelasSiswaBaru();
    }//GEN-LAST:event_btn_pilih_kelas_baruActionPerformed

    private void btn_batal_kelas_baruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batal_kelas_baruActionPerformed
        komp.closeDialogWarning(baru_kelas);
    }//GEN-LAST:event_btn_batal_kelas_baruActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog baru_kelas;
    private javax.swing.JButton btnBaru;
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JButton btn_batal_kelas_baru;
    private javax.swing.JButton btn_pilih_kelas_baru;
    private javax.swing.JTextField dNorek;
    private javax.swing.JPanel data;
    private javax.swing.JTextField filter_kelas_baru;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField pNama;
    private javax.swing.JTextField pSaldo;
    private javax.swing.JPanel panelData;
    private javax.swing.JPanel pasif;
    private javax.swing.JTextField tBukti;
    private javax.swing.JComboBox<String> tJenis;
    private javax.swing.JTextField tJumlah;
    private javax.swing.JTextField tKeterangan;
    private javax.swing.JLabel tNominalJumlah;
    private com.toedter.calendar.JDateChooser tTanggal;
    private javax.swing.JTable tabelTabungan;
    private javax.swing.JTable tabel_baru_kelas;
    private javax.swing.JPanel xxyyy;
    // End of variables declaration//GEN-END:variables

}
