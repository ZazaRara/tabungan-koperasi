/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpin;

import java.awt.event.KeyEvent;
import java.sql.Connection;
import utility.Item;
import utility.penangananDialog;
import utility.penangananKomponen;
import utility.saring_karakter;

/**
 *
 * @author iweks
 */
public class pinjaman extends javax.swing.JInternalFrame {

    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    String user = "";
    Connection kon;

    /**
     * Creates new form masterRombel
     */
    public pinjaman(String user, Connection ko) {

        this.user = user;
        this.kon = ko;
        initComponents();
    //    cariData();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);

//        btn_edit.setEnabled(false);
        //  btn_hapus.setEnabled(false);
    //    tmb_id.setVisible(false);

        komp.setJComboBoxListVektor(dJenis, "SELECT bunga, nama FROM master_pinjaman ORDER BY nama", "-- Pilih Pinjaman --", kon);

    }

    private void cariData() {
        String a = txt_filter.getText();
        String[] data = {"b.nama", "b.koin", "b.section", "a.namab"};
        String where = komp.getStringFilter(data, a);
        String sql = "Select a.koin, a.koin, a.nodok, b.nama, a.namab, "
                + "a.tg_tran, a.kon, FORMAT(a.bpinj,2) as ab, FORMAT(a.nilb,2) as nilb, "
                + "FORMAT(a.wajb,2) as wajb, FORMAT(a.wajiba,2) as wajiba, a.angsuran, "
                + "a.angsuran - ifnull(a.angke, 0) as sis "
                + "FROM masqom a LEFT JOIN masdin b ON b.koin = a.koin " + where + " AND a.angsuran != ifnull(a.angke,0) ORDER BY tg_tran LIMIT 200";
        //    System.out.println(sql);
        komp.setDataTabel(kon, tabelRombel, sql, 1);
//        btn_edit.setEnabled(false);
//        btn_hapus.setEnabled(false);
    }

    private void showKelasSiswaBaru() {
        String a = this.filter_kelas_baru.getText();
        String[] data = {"koin",
            "nama",
            "section"};

        String where = komp.getStringFilter(data, a);
        String sql = "SELECT koin, koin as kode, nama, section, bank, unit  FROM masdin " + where + " ORDER BY nama LIMIT 100";
        //  System.out.println(sql);
        komp.setDataTabel(kon, tabel_baru_kelas, sql, 1);
    }

    private void pilihKelasSiswaBaru() {
        int y = tabel_baru_kelas.getSelectedRow();
        if (y >= 0) {
            String id = tabel_baru_kelas.getValueAt(y, 1).toString();
            String nama = tabel_baru_kelas.getValueAt(y, 3).toString();
            String sec = tabel_baru_kelas.getValueAt(y, 4).toString();
            String bank = tabel_baru_kelas.getValueAt(y, 5).toString();
            dNorek.setText(id);
            dSection.setText(sec);
            dBank.setText(bank);
            dNama.setText(nama);
            showData();
            baru_kelas.dispose();

        } else {
            pesan.pesanError("ERROR", "Anggota belum dipilih", "Silahkan pilih anggota yang diinginkan");
        }
    }

    private void showData() {
        String nor = dNorek.getText().trim();
        String nama = dNama.getText().trim();
        if (nama.length() > 0) {
            String no = komp.getStringSQL(kon, "SELECT max(id) + 1 as jml FROM masqom");
            tJumlah.setText("0");
            tNominalJumlah.setText("0");
            dNodok.setText(nor.substring(nor.length() - 3, nor.length()) + "/" + no);
            dNorek.setEnabled(false);
            tNominalJumlah.setText("0");
            dAngsur.setText("1");
            dPotongGaji.setSelectedIndex(0);
            dBagian.setSelectedIndex(0);
            panelHitung.setVisible(false);
            String sql = "SELECT koin, namab, angsuran, angke FROM masqom WHERE koin = '" + nor + "' AND angsuran != ifnull(angke,0)";
            komp.setDataTabel(kon, riwayatPinjaman, sql, 1);
            dJenis.requestFocus();
        } else {
            pesan.pesanError("Nomor Rekening tidak ditemukan", "Periksa nomor rekening yang dimasukkan", "Silahkan ulangi lagi");
        }
    }

    private void hitungPinjaman() {
        String bunga = ((Item) dJenis.getSelectedItem()).getValue().toString();
        String jml = tJumlah.getText();
        String ke = dAngsur.getText();

        int pokok = Integer.parseInt(jml) / Integer.parseInt(ke);
        double bun = Double.parseDouble(jml) * (Double.parseDouble(bunga) / 100);
        int bungabulan = (int) bun;
        int total = Integer.parseInt(jml) + (Integer.parseInt(ke) * bungabulan);
        dBunga.setText(bunga);
        dAngusranWajib.setText(String.valueOf(pokok));
        dAngsuranBunga.setText(String.valueOf(bungabulan));
        dBesarAngsuran.setText(String.valueOf(pokok + bungabulan));
        dBesarTotal.setText(String.valueOf(total));
        panelHitung.setVisible(true);
    }

    private void simpanMasterKelas(boolean tutup) {

        String[][] data = {
            {"koin", dNorek.getText()},
            {"nodok", dNodok.getText()},
            {"namab", ((Item) dJenis.getSelectedItem()).getDescription().toString()},
            {"tg_tran", new java.sql.Date(dTgl.getDate().getTime()).toString()},
            {"wajiba", dBesarAngsuran.getText()},
            {"angsuran", dAngsur.getText()},
            {"kon", dPotongGaji.getSelectedItem().toString()},
            {"bunga", dBunga.getText()},
            {"nilb", dAngsuranBunga.getText()},
            {"wajb", dAngusranWajib.getText()},
            {"bpinj", tJumlah.getText()},
            {"tpinj", dBesarTotal.getText()},
            {"bank", dBank.getText()},
            {"ket", dBagian.getSelectedItem().toString()},};
        boolean ok = komp.simpanData(kon, "masqom", data, false);

        if (ok) {
            cariData();
            if (tutup) {
                pesan.pesanSukses("Sukses Simpan Data", "Pinjaman baru sukses disimpan.");
                tambah.dispose();
            } else {
                pesan.pesanSukses("Sukses Simpan Data", "Pinjaman baru sukses disimpan.");
                komp.hapusDataKomponen(jPanel1);
            }
        } else {
            pesan.pesanError("Gagal Simpan Data", "Pinjaman baru gagal disimpan.", "Hub. Administrator");
        }
    }

    private void updateMasterKelas(boolean tutup) {

      
    }

    private void showDataEdit(String id, String nama, String section, String nodok) {
        String sql = "Select namab, tg_tran,  FROM musik_master_list WHERE id = " + id;
        Object[] data = komp.setDataEdit(kon, sql);


    }

    private void deleteMasterKelas(String id) {

        boolean ok = komp.setSQL(kon, "UPDATE musik_master_list SET thru_date = now() WHERE id = " + id);
        //  boolean ok = komp.updateData(kon, "musik_master_list", data, "id = " + tmb_id.getText());

        if (ok) {
            cariData();
            pesan.pesanSukses("Sukses Update Data", "Master Pembayaran sukses dihapus.");

        } else {
            pesan.pesanError("Gagal Update Data", "Master Pembayaran gagal dihapus.", "Hub. Administrator");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tambah = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        dAngsur = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        dJenis = new javax.swing.JComboBox<>();
        btnCari = new javax.swing.JButton();
        dNorek = new javax.swing.JTextField();
        dSection = new javax.swing.JTextField();
        dNama = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        dTgl = new com.toedter.calendar.JDateChooser();
        tJumlah = new javax.swing.JTextField();
        tNominalJumlah = new javax.swing.JLabel();
        dBagian = new javax.swing.JComboBox<>();
        dPotongGaji = new javax.swing.JComboBox<>();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        panelHitung = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        dBunga = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        dAngusranWajib = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        dAngsuranBunga = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        dBesarAngsuran = new javax.swing.JTextField();
        dBesarTotal = new javax.swing.JTextField();
        dBank = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        riwayatPinjaman = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        dNodok = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        baru_kelas = new javax.swing.JDialog();
        jPanel14 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabel_baru_kelas = new javax.swing.JTable();
        filter_kelas_baru = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        btn_pilih_kelas_baru = new javax.swing.JButton();
        btn_batal_kelas_baru = new javax.swing.JButton();
        bayar = new javax.swing.JDialog();
        jPanel10 = new javax.swing.JPanel();
        bayarAngsuran = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        bayarKoin = new javax.swing.JTextField();
        bayarSection = new javax.swing.JTextField();
        bayarNama = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        bayarJumlah = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        bayarBank = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        riwayatPinjaman1 = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        bayarNodok = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        bayarJenis = new javax.swing.JTextField();
        bayarPinjam = new javax.swing.JTextField();
        bayarTanggal = new com.toedter.calendar.JDateChooser();
        jLabel56 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        bayarWajib = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        bayarBunga = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();
        bayarTotal = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        bayarAngsuranKe = new javax.swing.JTextField();
        jPanel18 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jButton5 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        xxxyy = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        tengah = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelRombel = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        txt_filter = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btn_cari = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        btn_tambah = new javax.swing.JButton();

        tambah.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        tambah.setModal(true);
        tambah.setUndecorated(true);
        tambah.setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 204, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        dAngsur.setDocument(new saring_karakter().setToUpperCase());
        dAngsur.setText("1");
        dAngsur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dAngsurKeyReleased(evt);
            }
        });
        jPanel1.add(dAngsur, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 220, 40, -1));

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel23.setText("Bagian :");
        jLabel23.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 120, 20));

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel24.setText("KOIN :");
        jLabel24.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 120, 20));

        dJenis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "INSTRUMEN", "PROGRAM", "KLASIFIKASI", "MENGENAL", "ALASAN_CUTI", "ALASAN_KELUAR" }));
        dJenis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dJenisActionPerformed(evt);
            }
        });
        jPanel1.add(dJenis, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, 220, -1));

        btnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/cari.png"))); // NOI18N
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });
        jPanel1.add(btnCari, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 20, -1, 30));

        dNorek.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        dNorek.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dNorekKeyReleased(evt);
            }
        });
        jPanel1.add(dNorek, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, 220, 30));

        dSection.setEnabled(false);
        jPanel1.add(dSection, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 150, -1));

        dNama.setEnabled(false);
        jPanel1.add(dNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 60, 380, -1));

        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel26.setText("Nama :");
        jLabel26.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 120, 20));

        dTgl.setDate(new java.util.Date());
        dTgl.setDateFormatString("dd/MM/yyyy");
        jPanel1.add(dTgl, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 120, -1, -1));

        tJumlah.setText("0");
        tJumlah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tJumlahKeyReleased(evt);
            }
        });
        jPanel1.add(tJumlah, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 180, 90, -1));

        tNominalJumlah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tNominalJumlah.setText("0");
        jPanel1.add(tNominalJumlah, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 200, 150, -1));

        dBagian.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "S", "J", "D" }));
        jPanel1.add(dBagian, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 280, 70, -1));

        dPotongGaji.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Y", "N" }));
        jPanel1.add(dPotongGaji, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 250, 70, -1));

        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel27.setText("Section :");
        jLabel27.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 120, 20));

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel28.setText("Jenis Pinjaman :");
        jLabel28.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 120, 20));

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel29.setText("Tanggal Pinjam :");
        jLabel29.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 120, 20));

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel30.setText("Besar Pinjaman :");
        jLabel30.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, 120, 20));

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("kali");
        jLabel31.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 220, 40, 20));

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel32.setText("Potong Gaji :");
        jLabel32.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 120, 20));

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel33.setText("Diangsur :");
        jLabel33.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 120, 20));

        panelHitung.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "  Perhitungan Cicilan  "));
        panelHitung.setOpaque(false);
        panelHitung.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel34.setText("Bunga :");
        jLabel34.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        panelHitung.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 100, 20));

        dBunga.setEditable(false);
        dBunga.setText("0");
        dBunga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dBungaKeyReleased(evt);
            }
        });
        panelHitung.add(dBunga, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 20, 40, -1));

        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel35.setText("Angsuran Wajib :");
        jLabel35.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        panelHitung.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 100, 20));

        dAngusranWajib.setEditable(false);
        dAngusranWajib.setText("0");
        dAngusranWajib.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dAngusranWajibKeyReleased(evt);
            }
        });
        panelHitung.add(dAngusranWajib, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 90, -1));

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel36.setText("Angsuran Bunga :");
        jLabel36.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        panelHitung.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 100, 20));

        dAngsuranBunga.setEditable(false);
        dAngsuranBunga.setText("0");
        dAngsuranBunga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dAngsuranBungaKeyReleased(evt);
            }
        });
        panelHitung.add(dAngsuranBunga, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 80, 90, -1));

        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel37.setText("Besar Angsuran :");
        jLabel37.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        panelHitung.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 100, 20));

        dBesarAngsuran.setEditable(false);
        dBesarAngsuran.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        dBesarAngsuran.setText("0");
        dBesarAngsuran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dBesarAngsuranKeyReleased(evt);
            }
        });
        panelHitung.add(dBesarAngsuran, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 110, 90, -1));

        dBesarTotal.setEditable(false);
        dBesarTotal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        dBesarTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        dBesarTotal.setText("0");
        dBesarTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dBesarTotalKeyReleased(evt);
            }
        });
        panelHitung.add(dBesarTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 180, -1));

        jPanel1.add(panelHitung, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 180, 240, 170));

        dBank.setEnabled(false);
        jPanel1.add(dBank, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 90, 150, -1));

        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel38.setText("Norek Bank :");
        jLabel38.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 90, 70, 20));

        riwayatPinjaman.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Nama", "Besar Angsuran", "Sisa Angsuran"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(riwayatPinjaman);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 40, 360, 270));

        jLabel9.setText("Daftar Pinjaman belum Lunas");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 20, 150, -1));
        jPanel1.add(dNodok, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 120, 150, -1));

        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel39.setText("No. Dok :");
        jLabel39.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 120, 70, 20));

        tambah.getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setBackground(new java.awt.Color(255, 51, 51));
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 51, 51));
        jLabel2.setText("PINJAMAN BARU");
        jPanel6.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 0, 390, 90));

        tambah.getContentPane().add(jPanel6, java.awt.BorderLayout.NORTH);

        jPanel7.setBackground(new java.awt.Color(255, 51, 51));
        jPanel7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton1.setText("Simpan");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton1);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton3.setText("Simpan & Tutup");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton3);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jButton2.setText("Tutup");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton2);

        tambah.getContentPane().add(jPanel7, java.awt.BorderLayout.SOUTH);

        baru_kelas.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        baru_kelas.setModal(true);
        baru_kelas.setUndecorated(true);
        baru_kelas.setResizable(false);

        jPanel14.setBackground(new java.awt.Color(255, 204, 255));
        jPanel14.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "  Data Kelas  "));
        jPanel17.setOpaque(false);
        jPanel17.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabel_baru_kelas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "2016-03-05", "07:00", "1", null, null, null}
            },
            new String [] {
                "No", "id", "KOIN", "Nama Anggota", "Section", "No. Rekening", "Unit"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabel_baru_kelas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabel_baru_kelasMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tabel_baru_kelas);

        jPanel17.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 700, 310));

        jPanel14.add(jPanel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 740, 350));

        filter_kelas_baru.setPreferredSize(new java.awt.Dimension(60, 25));
        filter_kelas_baru.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filter_kelas_baruKeyReleased(evt);
            }
        });
        jPanel14.add(filter_kelas_baru, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 350, -1));

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/reset.png"))); // NOI18N
        jButton4.setText("Filter");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel14.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, -1, -1));

        baru_kelas.getContentPane().add(jPanel14, java.awt.BorderLayout.CENTER);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jPanel15.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/icon.jpg"))); // NOI18N
        jPanel15.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, 90));

        jLabel13.setBackground(new java.awt.Color(255, 51, 51));
        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 51, 51));
        jLabel13.setText("Daftar Anggota");
        jPanel15.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 0, 390, 110));

        baru_kelas.getContentPane().add(jPanel15, java.awt.BorderLayout.NORTH);

        jPanel16.setBackground(new java.awt.Color(255, 51, 51));
        jPanel16.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        btn_pilih_kelas_baru.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        btn_pilih_kelas_baru.setText("Pilih");
        btn_pilih_kelas_baru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_pilih_kelas_baruActionPerformed(evt);
            }
        });
        jPanel16.add(btn_pilih_kelas_baru);

        btn_batal_kelas_baru.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        btn_batal_kelas_baru.setText("Tutup");
        btn_batal_kelas_baru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batal_kelas_baruActionPerformed(evt);
            }
        });
        jPanel16.add(btn_batal_kelas_baru);

        baru_kelas.getContentPane().add(jPanel16, java.awt.BorderLayout.SOUTH);

        bayar.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        bayar.setModal(true);
        bayar.setUndecorated(true);
        bayar.setResizable(false);

        jPanel10.setBackground(new java.awt.Color(255, 204, 255));
        jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bayarAngsuran.setDocument(new saring_karakter().setToUpperCase());
        bayarAngsuran.setText("1");
        bayarAngsuran.setEnabled(false);
        bayarAngsuran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bayarAngsuranKeyReleased(evt);
            }
        });
        jPanel10.add(bayarAngsuran, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 240, 40, -1));

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel41.setText("KOIN :");
        jLabel41.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 120, 20));

        bayarKoin.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bayarKoin.setEnabled(false);
        bayarKoin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bayarKoinKeyReleased(evt);
            }
        });
        jPanel10.add(bayarKoin, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, 220, 30));

        bayarSection.setEnabled(false);
        jPanel10.add(bayarSection, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 150, -1));

        bayarNama.setEnabled(false);
        jPanel10.add(bayarNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 60, 380, -1));

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel42.setText("Nama :");
        jLabel42.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 120, 20));

        bayarJumlah.setText("0");
        bayarJumlah.setEnabled(false);
        bayarJumlah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bayarJumlahKeyReleased(evt);
            }
        });
        jPanel10.add(bayarJumlah, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 180, 90, -1));

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel43.setText("Section :");
        jLabel43.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 120, 20));

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel44.setText("Jenis Pinjaman :");
        jLabel44.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 120, 20));

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel45.setText("Tanggal Pinjam :");
        jLabel45.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 120, 20));

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel46.setText("Jumlah Pinjaman :");
        jLabel46.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, 120, 20));

        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel47.setText("kali");
        jLabel47.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 240, 40, 20));

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel49.setText("Jumlah Angsuran :");
        jLabel49.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 240, 120, 20));

        bayarBank.setEnabled(false);
        jPanel10.add(bayarBank, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 90, 150, -1));

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel54.setText("Norek Bank :");
        jLabel54.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 90, 70, 20));

        riwayatPinjaman1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Nama", "Besar Angsuran", "Sisa Angsuran"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(riwayatPinjaman1);

        jPanel10.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 40, 360, 270));

        jLabel11.setText("Riwayat Pembayaran");
        jPanel10.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 20, 150, -1));

        bayarNodok.setEnabled(false);
        jPanel10.add(bayarNodok, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 120, 150, -1));

        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel55.setText("No. Dok :");
        jLabel55.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 120, 70, 20));

        bayarJenis.setEnabled(false);
        jPanel10.add(bayarJenis, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, 380, -1));

        bayarPinjam.setEnabled(false);
        jPanel10.add(bayarPinjam, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 120, 60, -1));

        bayarTanggal.setDate(new java.util.Date());
        bayarTanggal.setDateFormatString("dd/MM/yyyy");
        jPanel10.add(bayarTanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 270, -1, -1));

        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel56.setText("Tanggal Bayar :");
        jLabel56.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 120, 20));

        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel51.setText("Angsuran Wajib :");
        jLabel51.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 180, 100, 20));

        bayarWajib.setEditable(false);
        bayarWajib.setText("0");
        bayarWajib.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bayarWajibKeyReleased(evt);
            }
        });
        jPanel10.add(bayarWajib, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 180, 90, -1));

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel52.setText("Angsuran Bunga :");
        jLabel52.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 210, 100, 20));

        bayarBunga.setEditable(false);
        bayarBunga.setText("0");
        bayarBunga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bayarBungaKeyReleased(evt);
            }
        });
        jPanel10.add(bayarBunga, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 210, 90, -1));

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel53.setText("Besar Angsuran :");
        jLabel53.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, 100, 20));

        bayarTotal.setEditable(false);
        bayarTotal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bayarTotal.setText("0");
        bayarTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bayarTotalKeyReleased(evt);
            }
        });
        jPanel10.add(bayarTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 210, 90, -1));

        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel57.setText("Angsuran ke :");
        jLabel57.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel10.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 120, 20));

        bayarAngsuranKe.setDocument(new saring_karakter().setToUpperCase());
        bayarAngsuranKe.setText("1");
        bayarAngsuranKe.setEnabled(false);
        bayarAngsuranKe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bayarAngsuranKeKeyReleased(evt);
            }
        });
        jPanel10.add(bayarAngsuranKe, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 240, 40, -1));

        bayar.getContentPane().add(jPanel10, java.awt.BorderLayout.CENTER);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setBackground(new java.awt.Color(255, 51, 51));
        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 51, 51));
        jLabel6.setText("PEMBAYARAN");
        jPanel18.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 0, 390, 90));

        bayar.getContentPane().add(jPanel18, java.awt.BorderLayout.NORTH);

        jPanel19.setBackground(new java.awt.Color(255, 51, 51));
        jPanel19.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton5.setText("Simpan");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel19.add(jButton5);

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/simpan.png"))); // NOI18N
        jButton8.setText("Simpan & Tutup");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel19.add(jButton8);

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jButton9.setText("Tutup");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel19.add(jButton9);

        bayar.getContentPane().add(jPanel19, java.awt.BorderLayout.SOUTH);

        setBackground(java.awt.SystemColor.activeCaption);
        setBorder(null);
        setClosable(true);
        setTitle(".:: Pinjaman ::.");
        setToolTipText("");

        xxxyy.setBackground(new java.awt.Color(255, 255, 255));
        xxxyy.setLayout(new java.awt.BorderLayout());

        jPanel2.setOpaque(false);
        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel9.setBackground(new java.awt.Color(204, 255, 0));
        jPanel9.setOpaque(false);
        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setText("TRANSAKSI PIUTANG");
        jPanel9.add(jLabel4);

        jPanel2.add(jPanel9, java.awt.BorderLayout.CENTER);

        xxxyy.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        tengah.setBackground(new java.awt.Color(255, 255, 255));
        tengah.setOpaque(false);
        tengah.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setOpaque(false);

        tabelRombel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tabelRombel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "KOIN", "No. Dok", "Nama", "Nama Pinjaman", "Tanggal", "Potong Gaji", "Besar Pinjaman", "Bunga", "Wajib", "Besar Angsuran", "Lama cicilan", "Sisa cicilan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelRombel.setOpaque(false);
        tabelRombel.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tabelRombel.setShowHorizontalLines(false);
        tabelRombel.setShowVerticalLines(false);
        tabelRombel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelRombelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelRombel);

        tengah.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setOpaque(false);
        jPanel8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 5));

        txt_filter.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txt_filter.setForeground(new java.awt.Color(0, 0, 102));
        txt_filter.setPreferredSize(new java.awt.Dimension(350, 25));
        txt_filter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_filterKeyReleased(evt);
            }
        });
        jPanel8.add(txt_filter);

        jLabel3.setText("   ");
        jPanel8.add(jLabel3);

        btn_cari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/cari.png"))); // NOI18N
        btn_cari.setFocusable(false);
        btn_cari.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cari.setPreferredSize(new java.awt.Dimension(49, 26));
        btn_cari.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });
        jPanel8.add(btn_cari);

        jLabel8.setText("   ");
        jPanel8.add(jLabel8);

        tengah.add(jPanel8, java.awt.BorderLayout.NORTH);

        xxxyy.add(tengah, java.awt.BorderLayout.CENTER);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setOpaque(false);
        xxxyy.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setOpaque(false);
        xxxyy.add(jPanel4, java.awt.BorderLayout.LINE_END);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setOpaque(false);
        jPanel5.setPreferredSize(new java.awt.Dimension(80, 35));
        jPanel5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 8));

        jLabel5.setText("    ");
        jLabel5.setPreferredSize(new java.awt.Dimension(50, 20));
        jPanel5.add(jLabel5);

        btn_tambah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/tambah.png"))); // NOI18N
        btn_tambah.setText("PINJAM");
        btn_tambah.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        btn_tambah.setContentAreaFilled(false);
        btn_tambah.setFocusPainted(false);
        btn_tambah.setFocusable(false);
        btn_tambah.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_tambah.setMargin(new java.awt.Insets(5, 5, 2, 5));
        btn_tambah.setPreferredSize(new java.awt.Dimension(60, 45));
        btn_tambah.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_tambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tambahActionPerformed(evt);
            }
        });
        jPanel5.add(btn_tambah);

        xxxyy.add(jPanel5, java.awt.BorderLayout.LINE_START);

        getContentPane().add(xxxyy, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        komp.closeDialogWarning(tambah);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        simpanMasterKelas(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        simpanMasterKelas(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        cariData();
    }//GEN-LAST:event_btn_cariActionPerformed

    private void txt_filterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_filterKeyReleased

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cariData();
        }
    }//GEN-LAST:event_txt_filterKeyReleased

    private void btn_tambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tambahActionPerformed

        dNorek.setEnabled(true);
        komp.showDialog(tambah, 920, 540);
        komp.hapusDataKomponen(jPanel1);
    }//GEN-LAST:event_btn_tambahActionPerformed

    private void tabelRombelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelRombelMouseClicked
   //     btn_edit.setEnabled(true);
//         btn_hapus.setEnabled(true);
        if (evt.getClickCount() > 1) {
            int x = tabelRombel.getSelectedRow();
            String id = tabelRombel.getValueAt(x, 1).toString();

//            showDataEdit(id);

            komp.showDialog(bayar, 460, 240);
        }
//        btn_hapus.setEnabled(true);
    }//GEN-LAST:event_tabelRombelMouseClicked

    private void dJenisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dJenisActionPerformed

        //       hitungPinjaman();
    }//GEN-LAST:event_dJenisActionPerformed

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        showKelasSiswaBaru();
        komp.showDialog(baru_kelas, 760, 550);
    }//GEN-LAST:event_btnCariActionPerformed

    private void dNorekKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dNorekKeyReleased

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String id = dNorek.getText();
            String sql = "SELECT nama, section, bank FROM masdin WHERE koin = '" + id + "'";

            Object[] data = komp.setDataEdit(kon, sql);
            try {
//                tmb_nama1.setText(data[0].toString());
                String nama = data[0].toString();
                String sec = data[1].toString();
                String bank = data[2].toString();

                dSection.setText(sec);
                dBank.setText(bank);
                dNama.setText(nama);
            } catch (Exception e) {
            }
            showData();
        }
    }//GEN-LAST:event_dNorekKeyReleased

    private void tJumlahKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tJumlahKeyReleased
        hitungPinjaman();
        tNominalJumlah.setText(komp.ribuan(tJumlah.getText()));
    }//GEN-LAST:event_tJumlahKeyReleased

    private void dBungaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dBungaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_dBungaKeyReleased

    private void dAngusranWajibKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dAngusranWajibKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_dAngusranWajibKeyReleased

    private void dAngsuranBungaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dAngsuranBungaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_dAngsuranBungaKeyReleased

    private void dBesarAngsuranKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dBesarAngsuranKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_dBesarAngsuranKeyReleased

    private void tabel_baru_kelasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabel_baru_kelasMouseClicked

        int x = evt.getClickCount();
        if (x >= 2) {
            pilihKelasSiswaBaru();
        }
    }//GEN-LAST:event_tabel_baru_kelasMouseClicked

    private void filter_kelas_baruKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filter_kelas_baruKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            showKelasSiswaBaru();
        }
    }//GEN-LAST:event_filter_kelas_baruKeyReleased

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        showKelasSiswaBaru();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btn_pilih_kelas_baruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_pilih_kelas_baruActionPerformed

        pilihKelasSiswaBaru();
    }//GEN-LAST:event_btn_pilih_kelas_baruActionPerformed

    private void btn_batal_kelas_baruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batal_kelas_baruActionPerformed
        komp.closeDialogWarning(baru_kelas);
    }//GEN-LAST:event_btn_batal_kelas_baruActionPerformed

    private void dAngsurKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dAngsurKeyReleased
        hitungPinjaman();
    }//GEN-LAST:event_dAngsurKeyReleased

    private void dBesarTotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dBesarTotalKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_dBesarTotalKeyReleased

    private void bayarAngsuranKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bayarAngsuranKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_bayarAngsuranKeyReleased

    private void bayarKoinKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bayarKoinKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_bayarKoinKeyReleased

    private void bayarTotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bayarTotalKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_bayarTotalKeyReleased

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void bayarJumlahKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bayarJumlahKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_bayarJumlahKeyReleased

    private void bayarBungaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bayarBungaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_bayarBungaKeyReleased

    private void bayarWajibKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bayarWajibKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_bayarWajibKeyReleased

    private void bayarAngsuranKeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bayarAngsuranKeKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_bayarAngsuranKeKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog baru_kelas;
    private javax.swing.JDialog bayar;
    private javax.swing.JTextField bayarAngsuran;
    private javax.swing.JTextField bayarAngsuranKe;
    private javax.swing.JTextField bayarBank;
    private javax.swing.JTextField bayarBunga;
    private javax.swing.JTextField bayarJenis;
    private javax.swing.JTextField bayarJumlah;
    private javax.swing.JTextField bayarKoin;
    private javax.swing.JTextField bayarNama;
    private javax.swing.JTextField bayarNodok;
    private javax.swing.JTextField bayarPinjam;
    private javax.swing.JTextField bayarSection;
    private com.toedter.calendar.JDateChooser bayarTanggal;
    private javax.swing.JTextField bayarTotal;
    private javax.swing.JTextField bayarWajib;
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btn_batal_kelas_baru;
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_pilih_kelas_baru;
    private javax.swing.JButton btn_tambah;
    private javax.swing.JTextField dAngsur;
    private javax.swing.JTextField dAngsuranBunga;
    private javax.swing.JTextField dAngusranWajib;
    private javax.swing.JComboBox<String> dBagian;
    private javax.swing.JTextField dBank;
    private javax.swing.JTextField dBesarAngsuran;
    private javax.swing.JTextField dBesarTotal;
    private javax.swing.JTextField dBunga;
    private javax.swing.JComboBox<String> dJenis;
    private javax.swing.JTextField dNama;
    private javax.swing.JTextField dNodok;
    private javax.swing.JTextField dNorek;
    private javax.swing.JComboBox<String> dPotongGaji;
    private javax.swing.JTextField dSection;
    private com.toedter.calendar.JDateChooser dTgl;
    private javax.swing.JTextField filter_kelas_baru;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPanel panelHitung;
    private javax.swing.JTable riwayatPinjaman;
    private javax.swing.JTable riwayatPinjaman1;
    private javax.swing.JTextField tJumlah;
    private javax.swing.JLabel tNominalJumlah;
    private javax.swing.JTable tabelRombel;
    private javax.swing.JTable tabel_baru_kelas;
    private javax.swing.JDialog tambah;
    private javax.swing.JPanel tengah;
    private javax.swing.JTextField txt_filter;
    private javax.swing.JPanel xxxyy;
    // End of variables declaration//GEN-END:variables
}
