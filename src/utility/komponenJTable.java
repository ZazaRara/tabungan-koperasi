/*
 * komponenJTable.java
 *
 * Created on 25 Juli 2008, 19:38
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package utility;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.JTable;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Rara Kaltsum
 */
public class komponenJTable extends JTable implements Serializable{

    private Color warnaLatarGanjil = new Color(175,210,255);
 //    private Color warnaLatarGanjil = new Color(170,190,215);
    
    private Color warnaTextGanjil = Color.black;
    //  private Color warnaLatarGenap = javax.swing.UIManager.getDefaults().getColor("InternalFrame.inactiveTitleGradient");
//    private Color warnaLatarGenap = new Color(200,255,255);
    private Color warnaLatarGenap = new Color(145,200,235);
    private Color warnaTextGenap = Color.black;
    private Color rolloverBackColor = Color.yellow;
    private Color rolloverTextColor = Color.black;
    private int rolloverRowIndex = -1;
    public boolean editKolom = false;
    /** Creates a new instance of komponenJTable */
    public komponenJTable(DefaultTableModel tabTabel, boolean editKolom) {
        setModel(tabTabel);
        this.editKolom = editKolom;
        this.setSelectionBackground(Color.YELLOW);
        this.setSelectionForeground(Color.RED);
        //     this.setFont(new Font("Tahoma",1,12));
        daftar();
    }
    
    private void daftar(){
        RolloverListener listener = new RolloverListener();
        addMouseMotionListener(listener);
        addMouseListener(listener);
    }
    public boolean isCellEditable(int nRow, int nCol) {
        return editKolom;
    }
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);
        if (isRowSelected(row)) {
            c.setForeground(getSelectionForeground());
            c.setBackground(getSelectionBackground());
            c.setFont(new Font("Tahoma",0,11));
        } else if (row == rolloverRowIndex) {
            c.setForeground(rolloverTextColor);
            c.setBackground(rolloverBackColor);
            c.setFont(new Font("Tahoma",1,11));
        } else if (row % 2 == 0) {
            c.setForeground(warnaTextGanjil);
            c.setBackground(warnaLatarGanjil);
            c.setFont(new Font("Tahoma",0,11));
        } else {
            c.setForeground(warnaTextGenap);
            c.setBackground(warnaLatarGenap);
            c.setFont(new Font("Tahoma",0,11));
        }
        
        return c;
    }
    
    private class RolloverListener extends MouseInputAdapter {
        
        public void mouseExited(MouseEvent e) {
            rolloverRowIndex = -1;
            repaint();
        }
        
        public void mouseMoved(MouseEvent e) {
            int row = rowAtPoint(e.getPoint());
            if (row != rolloverRowIndex) {
                rolloverRowIndex = row;
                repaint();
            }
        }
    }
    
}
