package utility;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanelFix extends JPanel {

    private Image image = null;
    private boolean tile;

    public ImagePanelFix(Image image) {
        this.image = image;
    }

    public ImagePanelFix() {
        try {
            this.image = ImageIO.read(new File(System.getProperty("user.dir") + "\\image\\bg.png"));
        } catch (IOException ex) {
            Logger.getLogger(ImagePanelFix.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int iw = image.getWidth(this);
        int ih = image.getHeight(this);
        if (iw > 0 && ih > 0) {
           // for (int x = 0; x < getWidth(); x += iw) {
            //    for (int y = 0; y < getHeight(); y += ih) {
                    g.drawImage(image, 0, 0, iw, ih-80, this);
            //    }
          //  }
        }
    }
}
